package models

import "errors"

// As an aside, you might be wondering why we’re returning the ErrNoRecord error
// from our SnippetModel.Get() method, instead of sql.ErrNoRows directly. The
// reason is to help encapsulate the model completely, so that our application
// isn’t concerned with the underlying datastore or reliant on
// datastore-specific errors for its behavior.
var (
	ErrNoRecord           = errors.New("models: no matching record found")
	ErrInvalidCredentials = errors.New("models: invalid credentials")
	ErrDuplicateEmail     = errors.New("models: duplicate email")
)
