package models

import (
	"database/sql"
	"errors"
	"time"
)

// The internal directory is being used to hold ancillary non-application-
// specific code, which could potentially be reused. A database model which
// could be used by other applications in the future (like a command line
// interface application) fits the bill here.

type Snippet struct {
	ID      int
	Title   string
	Content string
	Created time.Time
	Expires time.Time
}

// DB.Query() is used for SELECT queries which return multiple rows.
// DB.QueryRow() is used for SELECT queries which return a single row.
// DB.Exec() is used for statements which don’t return rows (like INSERT and DELETE ).

// Define a SnippetModel type which wraps a sql.DB connection pool.
type SnippetModel struct {
	DB *sql.DB
}

// This will insert a new snippet into the database.
func (m *SnippetModel) Insert(title string, content string, expires int) (int, error) {
	stmt := `INSERT INTO snippets (title, content, created, expires) VALUES(?, ?, UTC_TIMESTAMP(), DATE_ADD(UTC_TIMESTAMP(), INTERVAL ? DAY))`

	// 	Behind the scenes, the DB.Exec() method works in three steps:
	// 1. It creates a new prepared statement on the database using the provided SQL
	// statement. The database parses and compiles the statement, then stores it
	// ready for execution.
	// 2. In a second separate step, Exec() passes the parameter values to the
	// database. The database then executes the prepared statement using these
	// parameters. Because the parameters are transmitted later, after the statement
	// has been compiled, the database treats them as pure data. They can’t change
	// the intent of the statement. So long as the original statement is not derived
	// from an untrusted data, injection cannot occur.
	// 3. It then closes (or deallocates) the prepared statement on the database.
	// The placeholder parameter syntax differs depending on your database. MySQL,
	// SQL Server and SQLite use the ? notation, but PostgreSQL uses the $N
	// notation.
	result, err := m.DB.Exec(stmt, title, content, expires)
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

// This will return a specific snippet based on its id.
func (m *SnippetModel) Get(id int) (*Snippet, error) {
	stmt := `SELECT id, title, content, created, expires FROM snippets
	WHERE expires > UTC_TIMESTAMP() AND id = ?`

	row := m.DB.QueryRow(stmt, id)

	s := &Snippet{}

	// Behind the scenes of rows.Scan() your driver will automatically convert the
	// raw output from the SQL database to the required native Go types. So long as
	// you’re sensible with the types that you’re mapping between SQL and Go, these
	// conversions should generally Just Work. Usually:
	// CHAR , VARCHAR and TEXT map to string .
	// BOOLEAN maps to bool .
	// INT maps to int ; BIGINT maps to int64 .
	// DECIMAL and NUMERIC map to float .
	// TIME , DATE and TIMESTAMP map to time.Time .
	// A quirk of our MySQL driver is that we need to use the parseTime=true parameter
	// in our DSN to force it to convert TIME and DATE fields to time.Time . Otherwise it returns
	// these as []byte objects. This is one of the many driver-specific parameters that it offers.
	err := row.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)

	if err != nil {
		// If the query returns no rows, then row.Scan() will return a
		// sql.ErrNoRows error. We use the errors.Is() function check for that
		// error specifically, and return our own ErrNoRecord error
		// instead (we'll create this in a moment).
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return s, nil
}

// This will return the 10 most recently created snippets.
func (m *SnippetModel) Latest() ([]*Snippet, error) {
	stmt := `SELECT id, title, content, created, expires FROM snippets
WHERE expires > UTC_TIMESTAMP() ORDER BY id DESC LIMIT 10`

	rows, err := m.DB.Query(stmt)
	if err != nil {
		return nil, err
	}

	// We defer rows.Close() to ensure the sql.Rows resultset is always properly
	// closed before the Latest() method returns. This defer statement should come
	// *after* you check for an error from the Query() method. Otherwise, if
	// Query() returns an error, you'll get a panic trying to close a nil
	// resultset.
	// Important: Closing a resultset with defer rows.Close() is critical in the
	// code above. As long as a resultset is open it will keep the underlying
	// database connection open… so if something goes wrong in this method and the
	// resultset isn’t closed, it can rapidly lead to all the connections in your
	// pool being used up.
	defer rows.Close()

	snippets := []*Snippet{}

	for rows.Next() {
		s := &Snippet{}
		// Use rows.Scan() to copy the values from each field in the row to the
		// new Snippet object that we created. Again, the arguments to row.Scan()
		// must be pointers to the place you want to copy the data into, and the
		// number of arguments must be exactly the same as the number of
		// columns returned by your statement.
		// Very roughly, the fix for this is to change the field that you’re are
		// scanning into from a string to a sql.NullString type. See this gist for a
		// working example.
		// But, as a rule, the easiest thing to do is simply avoid NULL values
		// altogether. Set NOT NULL constraints on all your database columns, like
		// we have done in this book, along with sensible DEFAULT values as
		// necessary.
		err = rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if err != nil {
			return nil, err
		}

		snippets = append(snippets, s)
	}

	// When the rows.Next() loop has finished we call rows.Err() to retrieve any
	// error that was encountered during the iteration. It's important to call
	// this - don't assume that a successful iteration was completed over the
	// whole resultset.
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return snippets, nil
}

// We need somewhere to store the prepared statement for the lifetime of our web
// application. A neat way is to embed in the model alongside the connection
// pool.
type TransactionModel struct {
	DB         *sql.DB
	InsertStmt *sql.Stmt
}

func NewTransactionModel(db *sql.DB) (*TransactionModel, error) {
	// Use the Prepare method to create a new prepared statement for the
	// current connection pool. This returns a sql.Stmt object which represents
	// the prepared statement.
	insertStmt, err := db.Prepare("INSERT INTO ...")
	if err != nil {
		return nil, err
	}
	// Store it in our model object, alongside the connection pool.
	return &TransactionModel{db, insertStmt}, nil
}

// You should always close the connection before the main function returns. So
// it is a good Idea to defer exampleModel.InsertStmt.Close() in main function.
// Prepared statements exist on database connections. So, because Go uses a pool
// of many database connections, what actually happens is that the first time a
// prepared statement (i.e. the sql.Stmt object) is used it gets created on a
// particular database connection. The sql.Stmt object then remembers which
// connection in the pool was used. The next time, the sql.Stmt object will
// attempt to use the same database connection again. If that connection is
// closed or in use (i.e. not idle) the statement will be re-prepared on another
// connection.
// Under heavy load, it’s possible that a large amount of prepared statements
// will be created on multiple connections. This can lead to statements being
// prepared and re-prepared more often than you would expect — or even running
// into server-side limits on the number of statements (in MySQL the default
// maximum is 16,382 prepared statements).
//func (m *TransactionModel) Insert(args...) error {
// Notice how we call Exec directly against the prepared statement, rather
// than against the connection pool? Prepared statements also support the
// Query and QueryRow methods.
//	_, err := m.InsertStmt.Exec(args...)
//	return err
//}

// It’s important to realize that calls to Exec() , Query() and QueryRow() can
// use any connection from the sql.DB pool. Even if you have two calls to Exec()
// immediately next to each other in your code, there is no guarantee that they
// will use the same database connection.
// Sometimes this isn’t acceptable. For instance, if you lock a table with
// MySQL’s LOCK TABLES command you must call UNLOCK TABLES on exactly the same
// connection to avoid a deadlock. To guarantee that the same connection is used
// you can wrap multiple statements in a transaction.
// Important: You must always call either Rollback() or Commit() before your function
// returns. If you don’t the connection will stay open and not be returned to
// the connection pool. This can lead to hitting your maximum connection
// limit/running out of resources. The simplest way to avoid this is to use
// defer tx.Rollback().
// Here’s the basic pattern:
func (m *TransactionModel) DoTransaction() error {
	// Calling the Begin() method on the connection pool creates a new sql.Tx
	// object, which represents the in-progress database transaction.
	tx, err := m.DB.Begin()
	if err != nil {
		return err
	}

	// Defer a call to tx.Rollback() to ensure it is always called before the
	// function returns. If the transaction succeeds it will be already be
	// committed by the time tx.Rollback() is called, making tx.Rollback() a
	// no-op. Otherwise, in the event of an error, tx.Rollback() will rollback
	// the changes before the function returns.
	defer tx.Rollback()

	// Call Exec() on the transaction, passing in your statement and any
	// parameters. It's important to notice that tx.Exec() is called on the
	// transaction object just created, NOT the connection pool. Although we're
	// using tx.Exec() here you can also use tx.Query() and tx.QueryRow() in
	// exactly the same way.
	_, err = tx.Exec("INSERT INTO ...")
	if err != nil {
		return err
	}
	// Carry out another transaction in exactly the same way.
	_, err = tx.Exec("UPDATE ...")
	if err != nil {
		return err
	}
	// If there are no errors, the statements in the transaction can be committed
	// to the database with the tx.Commit() method.
	err = tx.Commit()

	return err
}

// As I mentioned earlier, the Exec() , Query() and QueryRow() methods all use
// prepared statements behind the scenes to help prevent SQL injection attacks.
// They set up a prepared statement on the database connection, run it with the
// parameters provided, and then close the prepared statement.
// This might feel rather inefficient because we are creating and recreating the
// same prepared statements every single time.
// In theory, a better approach could be to make use of the DB.Prepare() method
// to create our own prepared statement once, and reuse that instead. This is
// particularly true for complex SQL statements (e.g. those which have multiple
// JOINS) and are repeated very often (e.g. a bulk insert of tens of thousands
// of records). In these instances, the cost of re-preparing statements may have
// a noticeable effect on run time.
func (m *TransactionModel) PreparedStatements() {

}
