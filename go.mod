module github.com/ravelsultan/let-us-go

go 1.20

require (
	github.com/go-playground/form v3.1.4+incompatible
	github.com/go-sql-driver/mysql v1.7.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/justinas/alice v1.2.0
)

require (
	github.com/alexedwards/scs/v2 v2.5.1 // indirect
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/justinas/nosurf v1.1.1 // indirect
	github.com/srinathgs/mysqlstore v0.0.0-20200417050510-9cbb9420fc4c // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
