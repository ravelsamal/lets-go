run:
	reflex -r "\.go$$" -s -- sh -c "go run ./cmd/web/"

up:
	docker compose up -d

down: 
	docker compose down
