package main

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/sessions"
	"github.com/julienschmidt/httprouter"
	"github.com/ravelsultan/let-us-go/internal/models"
	"github.com/ravelsultan/let-us-go/internal/validator"
)

type snippetCreateForm struct {
	Title               string `form:"title"`
	Content             string `form:"content"`
	Expires             int    `form:"expires"`
	validator.Validator `form:"-"`
}

type userLoginForm struct {
	Email               string `form:"email"`
	Password            string `form:"password"`
	validator.Validator `form:"-"`
}

func (app *application) isAuthenticated(r *http.Request) bool {
	session, err := app.sessionStore.Get(r, "authenticatedUserID")
	if err != nil {
		return false
	}

	if session.IsNew || session.Values["authenticatedUserID"] == "" {
		return false
	}

	return true
}

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	// if r.URL.Path != "/" {
	// 	app.notFound(w)
	// 	return
	// }

	// if r.Method != http.MethodGet {
	// 	app.clientError(w, http.StatusMethodNotAllowed)
	// 	return
	// }

	snippets, err := app.snippets.Latest()
	if err != nil {
		app.cfg.Log.Error.Println(err)
		app.serverError(w, err)
		return
	}

	// Call the newTemplateData() helper to get a templateData struct containing
	// the 'default' data (which for now is just the current year), and add the
	// snippets slice to it.
	data := app.newTemplateData(r)
	data.Snippets = snippets

	// Initialize a slice containing the paths to the two files. It's important to
	// note that the file containing our base template must be the *first* file in
	// the slice.
	// files := []string{
	// 	"./ui/html/base.tmpl.html",
	// 	"./ui/html/partials/nav.tmpl.html",
	// 	"./ui/html/partials/aside.tmpl.html",
	// 	"./ui/html/pages/home.tmpl.html",
	// }
	// ts, err := template.ParseFiles(files...)
	// if err != nil {
	// 	app.cfg.Log.Error.Println(err)
	// 	app.serverError(w, err)
	// 	return
	// }

	// data := templateData{
	// 	Snippets: snippets,
	// }

	// err = ts.ExecuteTemplate(w, "base", data)
	// if err != nil {
	// 	app.cfg.Log.Error.Println(err)
	// 	app.serverError(w, err)
	// 	return
	// }

	session, err := app.sessionStore.Get(r, "flash")
	if err != nil {
		app.serverError(w, err)
	}
	if !session.IsNew {
		data.Flash = session.Values["flash"].(string)
		session.Options.Path = "/"
		session.Options.MaxAge = -1 // deletes immediately
		session.Save(r, w)
	}

	app.render(w, http.StatusOK, "home.tmpl.html", data)
}

// When you’re using the Set() , Add() , Del() , Get() and Values() methods on
// the header map, the header name will always be canonicalized using the
// textproto.CanonicalMIMEHeaderKey() function. This converts the first letter
// and any letter following a hyphen to upper case, and the rest of the letters
// to lowercase. This has the practical implication that when calling these
// methods the header name is case-insensitive. If you need to avoid this
// canonicalization behavior you can edit the underlying header map directly (it
// has the type map[string][]string ). For example:
// w.Header()["X-XSS-Protection"] = []string{"1; mode=block"}
// Note: If a HTTP/2 connection is being used, Go will always automatically
// convert the header names and values to lowercase for you as per the HTTP/2
// specifications.

func (app *application) snippetView(w http.ResponseWriter, r *http.Request) {
	// Go uses http.DetectContentType() to set the Content-Type header based on
	// the body content.
	// But it identifies JSON as text/plain. If it cannot determine the type,
	// it sets to application/octet-stream.
	// if r.Method != http.MethodGet {
	// 	w.Header().Set("Allow", http.MethodGet)
	// 	app.clientError(w, http.StatusMethodNotAllowed)
	// 	return
	// }

	//snippetID, err := strconv.Atoi(r.URL.Query().Get("id"))
	params := httprouter.ParamsFromContext(r.Context())
	id, err := strconv.Atoi(params.ByName("id"))
	if err != nil || id < 1 {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Snippet is not found\n"))
		app.cfg.Log.Error.Println(err)
		return
	}

	snippet, err := app.snippets.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
		return
	}

	// Call the newTemplateData() helper to get a templateData struct containing
	// the 'default' data (which for now is just the current year), and add the
	// snippets slice to it.
	data := app.newTemplateData(r)
	data.Snippet = snippet

	session, err := app.sessionStore.Get(r, "flash")
	if err != nil {
		app.serverError(w, err)
		return
	}

	if !session.IsNew {
		data.Flash = session.Values["flash"].(string)
		session.Options.Path = "/snippet"
		session.Options.MaxAge = -1 // deletes immediately
		session.Save(r, w)
	}

	// files := []string{
	// 	"./ui/html/base.tmpl.html",
	// 	"./ui/html/partials/nav.tmpl.html",
	// 	"./ui/html/pages/view.tmpl.html",
	// }
	// // Parse the template files...
	// ts, err := template.ParseFiles(files...)
	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }

	// data := &templateData{Snippet: snippet}
	// // And then execute them. Notice how we are passing in the snippet
	// // data (a models.Snippet struct) as the final parameter?
	// err = ts.ExecuteTemplate(w, "base", data)
	// if err != nil {
	// 	app.serverError(w, err)
	// }

	app.render(w, http.StatusOK, "view.tmpl.html", data)
}

func (app *application) snippetCreatePost(w http.ResponseWriter, r *http.Request) {
	//if r.Method != http.MethodPost {
	// headers must be set before any call to WriteHeader or Write, otherwise
	// they won't have any effect.
	// w.Header().Set("Allow", http.MethodPost)
	// call to WriteHeader must always precede calls to Write, because
	// upon the first Write invocation, Go automatically send 200 OK header.
	// w.WriteHeader(http.StatusMethodNotAllowed)
	// w.Write([]byte("Method Not Allowed"))
	// return
	//}

	// title := "O snail"
	// content := "O snail\nClimb Mount Fuji,\nBut slowly, slowly!\n\n– Kobayashi Issa"
	// expires := 7

	// Unless you’re sending multipart data (i.e. your form has the
	// enctype="multipart/form-data" attribute) then POST , PUT and PATCH request
	// bodies are limited to 10MB. If this is exceeded then r.ParseForm() will
	// return an error. If you want to change this limit you can use the
	// http.MaxBytesReader() function like so:
	// r.Body = http.MaxBytesReader(w, r.Body, 4096)
	// err := r.ParseForm()
	// 	if err != nil {
	// 	http.Error(w, "Bad Request", http.StatusBadRequest)
	// 	return
	// }
	// With this code only the first 4096 bytes of the request body will be read
	// during r.ParseForm() . Trying to read beyond this limit will cause the
	// MaxBytesReader to return an error, which will subsequently be surfaced by
	// r.ParseForm() .
	// Additionally — if the limit is hit — MaxBytesReader sets a flag on
	// http.ResponseWriter which instructs the server to close the underlying TCP
	// connection.
	// err := r.ParseForm()
	// if err != nil {
	// 	app.cfg.Log.Error.Println(err)
	// 	app.clientError(w, http.StatusBadRequest)
	// 	return
	// }

	// The r.PostForm map is populated only for POST , PATCH and PUT requests, and
	// contains the form data from the request body.
	// In contrast, the r.Form map is populated for all requests (irrespective of
	// their HTTP method), and contains the form data from any request body and
	// any query string parameters. So, if our form was submitted to
	// /snippet/create?foo=bar , we could also get the value of the foo parameter
	// by calling r.Form.Get("foo") .
	// Note that in the event of a conflict, the request body value will take
	// precedent over the query string parameter.
	// Strictly speaking, the r.PostForm.Get() method only returns the first value
	// for a specific form field. This means you can’t use it with form fields
	// which potentially send multiple values, such as a group of checkboxes.
	// In this case you’ll need to work with the r.PostForm map directly. The
	// underlying type of the r.PostForm map is url.Values , which in turn has the
	// underlying type map[string][]string . So, for fields with multiple values
	// you can loop over the underlying map to access them like so:
	// for i, item := range r.PostForm["items"] {
	// fmt.Fprintf(w, "%d: Item %s\n", i, item)
	// }
	// title := r.PostForm.Get("title")
	// content := r.PostForm.Get("content")
	// expires, err := strconv.Atoi(r.PostForm.Get("expires"))
	// if err != nil {
	// 	app.clientError(w, http.StatusBadRequest)
	// 	return
	// }

	// form := snippetCreateForm{
	// 	Title:   title,
	// 	Content: content,
	// 	Expires: expires,
	// }

	var snippetForm snippetCreateForm
	// err = app.formDecoder.Decode(&snippetForm, r.PostForm)
	// if err != nil {
	// 	app.clientError(w, http.StatusBadRequest)
	// 	return
	// }

	err := app.decodePostForm(r, &snippetForm)
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	snippetForm.CheckField(validator.NotBlank(snippetForm.Title), "title", "This field cannot be blank")
	snippetForm.CheckField(validator.MaxChars(snippetForm.Title, 100), "title", "This field cannot exceed 100 characters")
	snippetForm.CheckField(validator.NotBlank(snippetForm.Content), "content", "this field cannot be blank")
	snippetForm.CheckField(validator.PermittedInt(snippetForm.Expires, 1, 7, 365), "expires", "This field must be equal 1, 7 or 365")

	if !snippetForm.Valid() {
		data := app.newTemplateData(r)
		data.Form = snippetForm
		app.render(w, http.StatusUnprocessableEntity, "create.tmpl.html", data)
		return
	}

	//id, err := app.snippets.Insert(title, content, expires)
	id, err := app.snippets.Insert(snippetForm.Title, snippetForm.Content, snippetForm.Expires)
	if err != nil {
		app.serverError(w, err)
		return
	}

	session, err := app.sessionStore.Get(r, "flash")
	if err != nil {
		app.serverError(w, err)
		fmt.Println(err)
		return
	}
	session.Values["flash"] = "Snippet successfully created!"
	session.Options = &sessions.Options{
		Path:     "/snippet",
		SameSite: http.SameSiteLaxMode,
		MaxAge:   0,
		HttpOnly: true,
		Secure:   true,
	}
	err = session.Save(r, w)
	if err != nil {
		app.serverError(w, err)
		fmt.Println(err)
		return
	}

	http.Redirect(w, r, fmt.Sprintf("/snippet/view/%d", id), http.StatusSeeOther)
}

func (app *application) snippetCreate(w http.ResponseWriter, r *http.Request) {
	data := app.newTemplateData(r)
	data.Form = snippetCreateForm{
		Expires: 365,
	}

	app.render(w, http.StatusOK, "create.tmpl.html", data)
}

type userSignupForm struct {
	Name                string `form:"name"`
	Email               string `form:"email"`
	Password            string `form:"password"`
	validator.Validator `form:"-"`
}

func (app *application) userSignup(w http.ResponseWriter, r *http.Request) {
	data := app.newTemplateData(r)
	data.Form = userSignupForm{}
	app.render(w, http.StatusOK, "signup.tmpl.html", data)
}

func (app *application) userLogin(w http.ResponseWriter, r *http.Request) {
	data := app.newTemplateData(r)
	data.Form = userLoginForm{}
	session, err := app.sessionStore.Get(r, "flash")
	if err != nil {
		app.serverError(w, err)
		return
	}
	if !session.IsNew {
		data.Flash = session.Values["flash"].(string)
		session.Options.Path = "/snippet"
		session.Options.MaxAge = -1 // deletes immediately
		session.Save(r, w)
	}
	app.render(w, http.StatusOK, "login.tmpl.html", data)
}

func (app *application) userSignupPost(w http.ResponseWriter, r *http.Request) {
	var form userSignupForm
	err := app.decodePostForm(r, &form)
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	// Validate the form contents using our helper functions.
	form.CheckField(validator.NotBlank(form.Name), "name", "This field cannot be blank")
	form.CheckField(validator.NotBlank(form.Email), "email", "This field cannot be blank")
	form.CheckField(validator.Matches(form.Email, validator.EmailRX), "email", "This field must be a valid email address")
	form.CheckField(validator.NotBlank(form.Password), "password", "This field cannot be blank")
	form.CheckField(validator.MinChars(form.Password, 8), "password", "This field must be at least 8 characters long")

	// If there are any errors, redisplay the signup form along with a 422
	// status code.
	if !form.Valid() {
		data := app.newTemplateData(r)
		data.Form = form
		app.render(w, http.StatusUnprocessableEntity, "signup.tmpl", data)
		return
	}

	// Try to create a new user record in the database. If the email already
	// exists then add an error message to the form and re-display it.
	err = app.users.Insert(form.Name, form.Email, form.Password)
	if err != nil {
		if errors.Is(err, models.ErrDuplicateEmail) {
			form.AddFieldError("email", "Email address is already in use")
			data := app.newTemplateData(r)
			data.Form = form
			app.render(w, http.StatusUnprocessableEntity, "signup.tmpl", data)
		} else {
			app.serverError(w, err)
		}
		return
	}

	session, err := app.sessionStore.Get(r, "flash")
	if err != nil {
		app.serverError(w, err)
		fmt.Println(err)
		return
	}
	session.Values["flash"] = "Your signup was successful. Please log in."
	session.Options = &sessions.Options{
		Path:     "/login",
		SameSite: http.SameSiteLaxMode,
		MaxAge:   0,
		HttpOnly: true,
		Secure:   true,
	}
	err = session.Save(r, w)
	if err != nil {
		app.serverError(w, err)
		fmt.Println(err)
		return
	}

	// And redirect the user to the login page.
	http.Redirect(w, r, "/user/login", http.StatusSeeOther)
}

func (app *application) userLoginPost(w http.ResponseWriter, r *http.Request) {
	// Decode the form data into the userLoginForm struct.
	var form userLoginForm
	err := app.decodePostForm(r, &form)
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}
	// Do some validation checks on the form. We check that both email and
	// password are provided, and also check the format of the email address as
	// a UX-nicety (in case the user makes a typo).
	form.CheckField(validator.NotBlank(form.Email), "email", "This field cannot be blank")
	form.CheckField(validator.Matches(form.Email, validator.EmailRX), "email", "This field must be a valid email address")
	form.CheckField(validator.NotBlank(form.Password), "password", "This field cannot be blank")
	if !form.Valid() {
		data := app.newTemplateData(r)
		data.Form = form
		app.render(w, http.StatusUnprocessableEntity, "login.tmpl", data)
		return
	}

	// Check whether the credentials are valid. If they're not, add a generic
	// non-field error message and re-display the login page.
	id, err := app.users.Authenticate(form.Email, form.Password)
	if err != nil {
		if errors.Is(err, models.ErrInvalidCredentials) {
			form.AddNonFieldError("Email or password is incorrect")
			data := app.newTemplateData(r)
			data.Form = form
			app.render(w, http.StatusUnprocessableEntity, "login.tmpl", data)
		} else {
			app.serverError(w, err)
		}
		return
	}

	session, err := app.sessionStore.Get(r, "authenticatedUserID")
	if err != nil {
		app.serverError(w, err)
		return
	}

	// Add the ID of the current user to the session, so that they are now
	// 'logged in'.
	session.Values["authenticatedUserID"] = id
	session.Save(r, w)

	// Redirect the user to the create snippet page.
	http.Redirect(w, r, "/snippet/create", http.StatusSeeOther)
}

func (app *application) userLogoutPost(w http.ResponseWriter, r *http.Request) {
	session, err := app.sessionStore.Get(r, "authenticatedUserID")
	if err != nil {
		app.serverError(w, err)
		return
	}

	session.Values["authenticatedUserID"] = ""
	session.Options.MaxAge = -1
	session.Save(r, w)

	flashSession, err := app.sessionStore.Get(r, "flash")
	if err != nil {
		app.serverError(w, err)
		return
	}
	flashSession.Values["flash"] = "You've been logged out successfully!"
	flashSession.Save(r, w)
	http.Redirect(w, r, "/", http.StatusSeeOther)
}
