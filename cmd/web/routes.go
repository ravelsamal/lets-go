package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/justinas/alice"
)

func (app *application) routes() http.Handler {
	r := httprouter.New()

	// Create a new middleware chain containing the middleware specific to our
	// dynamic application routes. For now, this chain will only contain the
	// LoadAndSave session middleware but we'll add more to it later.
	r.Handler(http.MethodGet, "/", noSurf(app.home))
	r.HandlerFunc(http.MethodGet, "/snippet/view/:id", app.snippetView)
	r.HandlerFunc(http.MethodGet, "/snippet/create", app.requireAuthentication(app.snippetCreate))
	r.HandlerFunc(http.MethodPost, "/snippet/create", app.requireAuthentication(app.snippetCreatePost))

	r.Handler(http.MethodGet, "/user/signup", noSurf(app.userSignup))
	r.Handler(http.MethodPost, "/user/signup", noSurf(app.userSignupPost))
	r.Handler(http.MethodGet, "/user/login", noSurf(app.userLogin))
	r.Handler(http.MethodPost, "/user/login", noSurf(app.userLoginPost))
	r.Handler(http.MethodPost, "/user/logout", noSurf(app.userLogoutPost))

	// r.HandlerFunc(http.MethodGet, "/", app.home)
	// r.HandlerFunc(http.MethodGet, "/snippet/view/:id", app.snippetView)
	// r.HandlerFunc(http.MethodGet, "/snippet/create", app.snippetCreate)
	// r.HandlerFunc(http.MethodPost, "/snippet/create", app.snippetCreatePost)

	// Create a handler function which wraps our notFound() helper, and then
	// assign it as the custom handler for 404 Not Found responses. You can also
	// set a custom handler for 405 Method Not Allowed responses by setting
	// router.MethodNotAllowed in the same way too.
	r.NotFound = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		app.notFound(w)
	})

	// mux := http.NewServeMux()
	// subtree path
	// mux.HandleFunc("/", app.home)

	// fixed path
	// mux.HandleFunc("/snippet/view", app.snippetView)
	// mux.HandleFunc("/snippet/create", app.snippetCreate)

	// Go’s file server has a few really nice features that are worth mentioning:
	// It sanitizes all request paths by running them through the path.Clean()
	// function before searching for a file. This removes any . and .. elements
	// from the URL path, which helps to stop directory traversal attacks. This
	// feature is particularly useful if you’re using the fileserver in
	// conjunction with a router that doesn’t automatically sanitize URL paths.
	// Range requests are fully supported. This is great if your application is
	// serving large files and you want to support resumable downloads. You can
	// see this functionality in action if you use curl to request bytes 100-199
	// of the logo.png file, like so:
	// curl -i -H "Range: bytes=100-199" --output - lh:4000/static/img/logo.png
	// The Last-Modified and If-Modified-Since headers are transparently
	// supported. If a file hasn’t changed since the user last requested it, then
	// http.FileServer will send a 304 Not Modified status code instead of the
	// file itself. This helps reduce latency and processing overhead for both the
	// client and server. The Content-Type is automatically set from the file
	// extension using the mime.TypeByExtension() function. You can add your own
	// custom extensions and content types using the mime.AddExtensionType()
	// function if necessary.
	// It’s important to note that, once the application is up-and-running,
	// http.FileServer probably won’t be reading these files from disk. Both
	// Windows and Unix-based operating systems cache recently-used files in RAM,
	// so (for frequently-served files at least) it’s likely that http.FileServer
	// will be serving them from RAM rather than making the relatively slow
	// round-trip to your hard disk.
	fileServer := http.FileServer(http.Dir(app.cfg.StaticDir))
	//mux.Handle("/static/", http.StripPrefix("/static", fileServer))
	r.Handler(http.MethodGet, "/static/*filepath", http.StripPrefix("/static", fileServer))

	// Serving single files
	// Sometimes you might want to serve a single file from within a handler. For
	// this there’s the http.ServeFile() function, which you can use like so:
	downloadHandler := func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./ui/static/file.zip")
	}
	//mux.HandleFunc("/download", downloadHandler)
	r.HandlerFunc(http.MethodGet, "/download", downloadHandler)

	// You can include host names
	//mux.HandleFunc("rava.test/snippet/view", snippetView)
	//mux.HandleFunc("localhost/snippet/create", snippetCreate)

	// Create a middleware chain containing our 'standard' middleware
	// which will be used for every request our application receives.
	standard := alice.New(app.recoverPanic, app.logRequest, secureHeaders)

	//return standard.Then(mux)
	return standard.Then(r)
}
