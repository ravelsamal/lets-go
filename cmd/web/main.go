package main

import (
	"crypto/tls"
	"database/sql"
	"flag"
	"html/template"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-playground/form"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	"github.com/ravelsultan/let-us-go/internal/models"
)

type Log struct {
	Error *log.Logger
	Info  *log.Logger
}

type Config struct {
	HTTPAddr  string
	StaticDir string
	DSN       string
	Log       *Log
}

type application struct {
	cfg           *Config
	snippets      *models.SnippetModel
	users         *models.UserModel
	templateCache map[string]*template.Template
	formDecoder   *form.Decoder
	sessionStore  *sessions.CookieStore
}

func main() {

	cfg := Config{}
	flag.StringVar(&cfg.HTTPAddr, "addr", ":8080", "HTTP network address")
	flag.StringVar(&cfg.StaticDir, "static-dir", "./ui/static/", "Path to static assets")
	flag.StringVar(&cfg.DSN, "dsn", "root:rava@tcp(127.0.0.1:3306)/snippetbox?parseTime=true", "Data source name for the database connection")
	flag.Parse()
	cfg.Log = &Log{
		Error: log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile),
		Info:  log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime),
	}

	db, err := openDB(cfg.DSN)
	if err != nil {
		cfg.Log.Error.Fatal(err)
	}

	defer db.Close()

	templateCache, err := newTemplateCache()
	if err != nil {
		cfg.Log.Error.Fatal(err)
	}

	formDecoder := form.NewDecoder()

	sessionStore := sessions.NewCookieStore([]byte("iljfkejfiltkjeiosdjgbligesdwdjji"))

	// sessionManager := scs.New()
	// sessionManager.Store = mysqlstore.New(db, time.Duration(50))
	// sessionManager.Lifetime = 12 * time.Hour

	app := application{
		cfg:           &cfg,
		snippets:      &models.SnippetModel{DB: db},
		users:         &models.UserModel{DB: db},
		templateCache: templateCache,
		formDecoder:   formDecoder,
		sessionStore:  sessionStore,
	}

	// One change, which is almost always a good idea to make, is to restrict the
	// elliptic curves that can potentially be used during the TLS handshake. Go
	// supports a few elliptic curves, but as of Go 1.18 only tls.CurveP256 and
	// tls.X25519 have assembly implementations. The others are very CPU
	// intensive, so omitting them helps ensure that our server will remain
	// performant under heavy loads.
	// For some applications, it may be desirable to limit your HTTPS server to
	// only support some of these cipher suites. For example, you might want to
	// only support cipher suites which use ECDHE (forward secrecy) and not
	// support weak cipher suites that use RC4, 3DES or CBC. You can do this via
	// the tls.Config.CipherSuites field.
	// In our code we’ve also set the ReadTimeout setting to 5 seconds. This means
	// that if the request headers or body are still being read 5 seconds after the
	// request is first accepted, then Go will close the underlying connection.
	// Because this is a ‘hard’ closure on the connection, the user won’t receive any
	// HTTP(S) response. Setting a short ReadTimeout period helps to mitigate the
	// risk from slow-client attacks — such as Slowloris — which could otherwise keep
	// a connection open indefinitely by sending partial, incomplete, HTTP(S)
	// requests.
	// Important: If you set ReadTimeout but don’t set IdleTimeout , then
	// IdleTimeout will default to using the same setting as ReadTimeout . For
	// instance, if you set ReadTimeout to 3 seconds, then there is the
	// side-effect that all keep-alive connections will also be closed after 3
	// seconds of inactivity. Generally, my recommendation is to avoid any
	// ambiguity and always set an explicit IdleTimeout value for your server.
	// The WriteTimeout setting will close the underlying connection if our server
	// attempts to write to the connection after a given period (in our code, 10
	// seconds). But this behaves slightly differently depending on the protocol
	// being used. For HTTP connections, if some data is written to the connection
	// more than 10 seconds after the read of the request header finished, Go will
	// close the underlying connection instead of writing the data. For HTTPS
	// connections, if some data is written to the connection more than 10 seconds
	// after the request is first accepted, Go will close the underlying
	// connection instead of writing the data. This means that if you’re using
	// HTTPS (like we are) it’s sensible to set WriteTimeout to a value greater
	// than ReadTimeout . It’s important to bear in mind that writes made by a
	// handler are buffered and written to the connection as one when the handler
	// returns. Therefore, the idea of WriteTimeout is generally not to prevent
	// long-running handlers, but to prevent the data that the handler returns
	// from taking too long to write.
	// The http.Server object also provides a ReadHeaderTimeout setting, which we
	// haven’t used in our application. This works in a similar way to ReadTimeout ,
	// except that it applies to the read of the HTTP(S) headers only. So, if you set
	// ReadHeaderTimeout to 3 seconds a connection will be closed if the request
	// headers are still being read 3 seconds after the request is accepted. However,
	// reading of the request body can still take place after 3 seconds has passed,
	// without the connection being closed.
	// This can be useful if you want to apply a server-wide limit to reading request
	// headers, but want to implement different timeouts on different routes when it
	// comes to reading the request body (possibly using the http.TimeoutHandler()
	// middleware).
	s := http.Server{
		Addr:     cfg.HTTPAddr,
		Handler:  app.routes(),
		ErrorLog: cfg.Log.Error,
		TLSConfig: &tls.Config{
			CurvePreferences: []tls.CurveID{tls.X25519, tls.CurveP256},
			CipherSuites: []uint16{
				tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
				tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
				tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			},
		},
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	cfg.Log.Info.Printf("Starting the HTTP server on %s", cfg.HTTPAddr)
	err = s.ListenAndServeTLS("./tls/cert.pem", "./tls/key.pem")
	cfg.Log.Error.Fatal(err)
}

func openDB(dsn string) (*sql.DB, error) {
	// The sql.Open() function initializes a new sql.DB object, which is
	// essentially a pool of database connections.
	// The sql.Open() function returns a sql.DB object. This isn’t a database
	// connection — it’s a pool of many connections. This is an important
	// difference to understand. Go manages the connections in this pool as
	// needed, automatically opening and closing connections to the database via
	// the driver. The connection pool is safe for concurrent access, so you can
	// use it from web application handlers safely.
	// The connection pool is intended to be long-lived. In a web application it’s
	// normal to initialize the connection pool in your main() function and then
	// pass the pool to your handlers. You shouldn’t call sql.Open() in a
	// short-lived handler itself — it would be a waste of memory and network
	// resources.
	// The first parameter to sql.Open() is the driver name and the second
	// parameter is the data source name (sometimes also called a connection
	// string or DSN) which describes how to connect to your database.
	// The parseTime=true part of the DSN above is a driver-specific parameter
	// which instructs our driver to convert SQL TIME and DATE fields to Go
	// time.Time objects.
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}
